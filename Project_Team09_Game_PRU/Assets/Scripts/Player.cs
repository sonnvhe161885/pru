﻿using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveSpeed = 5f;

    public Rigidbody2D rb;
    public SpriteRenderer characterSR;
    Animator animator;

    public float dashBoost = 2f;
    private float dashTime;
    public float DashTime;
    private bool once;

    public Vector3 moveInput;

    public GameObject damPopUp;
    public LosePanel losePanel;

    private PolygonCollider2D mapBoundary; // Thêm biến để lưu trữ Polygon Collider của viền map

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponentInChildren<Animator>();
        mapBoundary = GameObject.Find("CM Confiner").GetComponent<PolygonCollider2D>(); // Lấy Polygon Collider của viền map
    }


    // Update is called once per frame
    void Update()
    {
        /// Part 2
        // Movement
        moveInput.x = Input.GetAxisRaw("Horizontal");
        moveInput.y = Input.GetAxisRaw("Vertical");

        // Tính toán vị trí mới
        Vector3 newPosition = transform.position + moveSpeed * Time.deltaTime * moveInput;

        // Kiểm tra nếu vị trí mới nằm trong giới hạn của Polygon Collider
        if (IsWithinBounds(newPosition))
        {
            transform.position = newPosition; // Cập nhật vị trí nếu nằm trong giới hạn
        }

        //

        animator.SetFloat("Speed", moveInput.sqrMagnitude);

        if (Input.GetKeyDown(KeyCode.Space) && dashTime <= 0)
        {
            animator.SetBool("Roll", true);
            moveSpeed += dashBoost;
            dashTime = DashTime;
            once = true;
        }

        if (dashTime <= 0 && once)
        {
            animator.SetBool("Roll", false);
            moveSpeed -= dashBoost;
            once = false;
        }
        else
        {
            dashTime -= Time.deltaTime;
        }

        // Rotate Face
        if (moveInput.x != 0)
            if (moveInput.x < 0)
                characterSR.transform.localScale = new Vector3(-1, 1, 0);
            else
                characterSR.transform.localScale = new Vector3(1, 1, 0);
    }

    private bool IsWithinBounds(Vector3 position)
    {
        return mapBoundary.OverlapPoint(new Vector2(position.x, position.y));
    }

    public void TakeDamageEffect(int damage)
    {
        if (damPopUp != null)
        {
            GameObject instance = Instantiate(damPopUp, transform.position
                    + new Vector3(UnityEngine.Random.Range(-0.3f, 0.3f), 0.5f, 0), Quaternion.identity);
            instance.GetComponentInChildren<TextMeshProUGUI>().text = damage.ToString();
            Animator animator = instance.GetComponentInChildren<Animator>();
            animator.Play("red");
        }
        if (GetComponent<Health>().isDead)
        {
            losePanel.Show();
        }
    }
}
